﻿using System;
using System.Windows.Forms;
using Timer = System.Windows.Forms.Timer;

namespace AccessSplitter
{
    public partial class frmMain : Form
    {
        private Timer _timer;

        private ProcessExecutor _processExecutor;

        private string _path;
        
        public frmMain()
        {
            InitializeComponent();
            _timer = new Timer
            {
                Enabled = false,
                Interval = 10
            };
            _timer.Tick += TimerOnTick;
        }

        private void TimerOnTick(object sender, EventArgs e) => pbProgress.Value = _processExecutor.ProgressValue;

        private void btns_Click(object sender, EventArgs e)
        {
             Button btn = sender as Button;

             switch (btn?.Name)
             {
                 case "btnParcourir":
                     if (ofdFile.ShowDialog() == DialogResult.OK)
                     {
                         _path = ofdFile.FileName;
                         btnProcess.Enabled = btnAfficher.Enabled = pbProgress.Enabled = _path != null;
                     }
                     break;
                 case "btnProcess":

                     if (_path != null)
                     {
                         _processExecutor = new ProcessExecutor(_path);
                         btnProcess.Enabled = btnParcourir.Enabled = btnAfficher.Enabled = false;
                         _processExecutor.Finished += ProcessExecutorFinished;
                         _timer.Start();
                     }
                     else MessageBox.Show("Vous n'avez pas ouvert de fichier.");
                     
                     break;
                 case "btnAfficher":
                     if (_processExecutor != null)
                        ShowResult.Show(_processExecutor.Datas);
                     break;
                     
             }
        }

        private void ProcessExecutorFinished(object sender, EventArgs e)
        {
            _timer.Stop();
            
            // Permet d'executer ce qui suit dans le thread principale et non sur celui de ProcessExecutor
            btnProcess.BeginInvoke(new Action(() =>
            {
                pbProgress.Value = 100;
                btnProcess.Enabled = btnParcourir.Enabled = btnAfficher.Enabled = true;
                MessageBox.Show("Processus terminé !");
            }));
        }
    }
}