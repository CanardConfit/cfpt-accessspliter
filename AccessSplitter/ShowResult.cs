﻿using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CefSharp.Web;
using CefSharp.WinForms;

namespace AccessSplitter
{
    public partial class ShowResult : Form
    {
        private readonly List<LogContainer> _logContainers;
        
        private ShowResult(List<LogContainer> logContainers)
        {
            _logContainers = logContainers;
            InitializeComponent();
            
            Init();
        }

        private void Init()
        {
            
            if (_logContainers != null)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("let mark;");
                
                /*
                mark = new mapboxgl.Marker({ color: 'black' }).setLngLat([-119.7993392944336,47.206031799316406]);
                mark.getElement().addEventListener('click', function () {document.getElementById('content-popup').innerHTML = "<h1>13.66.139.116</h1><table><tr><th>Date</th><th>HTTP Version</th><th>Request Type</th><th>HTTP Code</th><th>User Agent</th><th>URL</th></tr><tr><td>03.02.2021 00:38:04</td><td>HTTP/1.1</td><td>GET</td><td>200</td><td>Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)</td><td>/template/assets/vendor/appear.js</td></tr></table>";$('.hover_bkgr_fricc').show();resize();});
                mark.addTo(mymap);
                 */
                
                foreach (string ip in _logContainers.Select(log => log.Ip).Distinct())
                {
                    LogContainer logIpLoc = _logContainers.First(log => log.Ip == ip);
                    List<LogContainer> logsIp = _logContainers.Where(log => log.Ip == ip).ToList();

                    string color = logsIp.Count switch
                    {
                        > 10 => "red",
                        > 6 => "orange",
                        _ => "green"
                    };

                    sb.AppendLine("mark = new mapboxgl.Marker({color:'" + color +"' }).setLngLat([" + logIpLoc.Loc.Longitude.ToString(CultureInfo.InvariantCulture) + "," + logIpLoc.Loc.Latitude.ToString(CultureInfo.InvariantCulture) + "]);");

                    StringBuilder table = new StringBuilder();
                    
                    table.Append("<table class='table table-striped'>");
                    table.Append("<tr>");
                    table.Append("<th>Date</th>");
                    table.Append("<th>HTTP Version</th>");
                    table.Append("<th>Request Type</th>");
                    table.Append("<th>HTTP Code</th>");
                    table.Append("<th>User Agent</th>");
                    table.Append("<th>URL</th>");
                    table.Append("</tr>");
                    foreach (LogContainer logContainer in logsIp)
                    {
                        table.Append("<tr>");
                        table.Append($"<td>{logContainer.Date:dd/MM/yyyy HH:mm:ss}</td>");
                        table.Append($"<td>{logContainer.Httpversion}</td>");
                        table.Append($"<td>{logContainer.Requesttype}</td>");
                        table.Append($"<td>{logContainer.HttpCode}</td>");
                        table.Append($"<td>{logContainer.Useragent}</td>");
                        table.Append($"<td>{logContainer.Url}</td>");
                        table.Append("</tr>");
                    }
                    table.Append("</table>");
                    
                    sb.AppendLine("mark.getElement().addEventListener('click', function () {document.getElementById('content-popup-title').innerHTML = \"<div style='color:" + color + ";'>" + ip + "</div>\";document.getElementById('content-popup').innerHTML = \"" + table + "\";$(\"#myModal\").modal('show');});");
                    sb.AppendLine("mark.addTo(mymap);");
                }

                var browser =
                    new ChromiumWebBrowser(new HtmlString(Resources.htmlMapPage.Replace("%[JSTOAPPLY]%", sb.ToString())))
                    {
                        Location = new Point(0, 0), Size = Size
                    };


                Controls.Add(browser);
            }
        }

        public static void Show(List<LogContainer> datas)
        {
            ShowResult result = new ShowResult(datas);
            result.ShowDialog();
        }
    }
}