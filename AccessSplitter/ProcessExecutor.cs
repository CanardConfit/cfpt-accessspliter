﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using Newtonsoft.Json;

namespace AccessSplitter
{
    public class ProcessExecutor
    {
        private int _progressValue;

        public EventHandler Finished;
        
        private readonly List<LogContainer> _datas = new();
        
        private readonly string _path;

        private readonly Regex _ipPattern = new("^.*(?= - - )");
        private readonly Regex _datePattern = new("(?<= - - \\[).*(?= \\+0?[0-9]00\\])");
        private readonly Regex _urlGroupPattern = new("(?<=\").*(?=\" ((?<=\" )[0-9]{3}(?= )))");
        private readonly Regex _requesttypePattern = new("^((?<=).*(?= \\/))");
        private readonly Regex _urlPattern = new("(?<=((?<=).*(?= \\/)) ).*(?= (HTTP\\/[0-9]{1,2}\\.[0-9]{1,2}))");
        private readonly Regex _matchMissingPattern = new(".*");
        private readonly Regex _httpversionPattern = new("(HTTP\\/[0-9]{1,2}\\.[0-9]{1,2})$");
        private readonly Regex _httpcodePattern = new("(?<=\" )[0-9]{3}(?= )");
        private readonly Regex _useragentPattern = new("(?<=\".*\" \").*(?=\")");

        public ProcessExecutor(string path)
        {
            _path = path;
            _progressValue = 0;

            Thread processThread = new Thread(Process);
            processThread.Start();
        }

        public int ProgressValue => _progressValue;

        public List<LogContainer> Datas => _datas;

        private void Process()
        {
            string[] lines = File.ReadAllLines(_path);
            for (var lineI = 0; lineI < lines.Length; lineI++)
            {
                string line = lines[lineI].Replace("\\x", "\\\\x");
                string ipStr = _ipPattern.Match(line).Value;
                string dateStr = _datePattern.Match(line).Value;
                string httpcodeStr = _httpcodePattern.Match(line).Value;
                string useragentStr = _useragentPattern.Match(line).Value;

                if (ipStr != "" && dateStr != "" && httpcodeStr != "" && useragentStr != "")
                {
                    string urlContent = _urlGroupPattern.Match(line).Value;

                    string urlStr = _urlPattern.Match(urlContent).Value;
                    string requestStr = _requesttypePattern.Match(urlContent).Value;
                    string httpversionStr = _httpversionPattern.Match(urlContent).Value;

                    if (urlStr == "")
                        urlStr = _matchMissingPattern.Match(urlContent).Value;
                    if (requestStr == "")
                        requestStr = "Inconnu";
                    if (httpversionStr == "")
                        httpversionStr = "Inconnu";

                    if (urlStr != "" && requestStr != "" && httpversionStr != "")
                    {
                        if (int.TryParse(httpcodeStr, out int httpcode))
                        {
                            DateTime date = DateTime.ParseExact(dateStr, "dd/MMM/yyyy:HH:mm:ss",
                                CultureInfo.InvariantCulture);

                            Datas.Add(new LogContainer(ipStr, null, date, urlStr, httpcode, useragentStr, requestStr,
                                httpversionStr));
                        }
                    }
                }

                _progressValue = (int)Math.Round((lineI+1.0) / lines.Length * 20);
            }

            List<string> ips = Datas.Select(el => el.Ip).Distinct().ToList();

            for (var ipI = 0; ipI < ips.Count; ipI++)
            {
                string ip = ips[ipI];
                GeoCoordinate loc = GetIpInfo(ip);

                IEnumerable<LogContainer> list = Datas.Where(el => el.Ip == ip);

                foreach (LogContainer container in list)
                    container.Loc = loc;
                
                _progressValue = (int)Math.Round((ipI+1.0) / ips.Count * 80.0 + 20.0);
            }

            Finished.Invoke(this, EventArgs.Empty);
        }
        
        private GeoCoordinate GetIpInfo(string ip)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"http://api.ipstack.com/{ip}?access_key=fef694d96679b996f6ea675b7899270a&format=1");
            request.Method = "GET";

            dynamic response = JsonConvert.DeserializeObject(new StreamReader(request.GetResponse().GetResponseStream()!).ReadToEnd());
            
            return response != null ? new GeoCoordinate(GetDouble(response.latitude.ToString().Replace(",", ".")), GetDouble(response.longitude.ToString().Replace(",", "."))) : null;
        }
        
        public static double GetDouble(string value)
        {
            double result;

            //Try parsing in the current culture
            if (!double.TryParse(value, NumberStyles.Any, CultureInfo.CurrentCulture, out result) &&
                //Then try in US english
                !double.TryParse(value, NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out result) &&
                //Then in neutral language
                !double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                throw new Exception($"Impossible de convertir le string {value} en double.");
            }

            return result;
        }
    }
}