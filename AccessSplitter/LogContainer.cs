﻿using System;
using System.Device.Location;

namespace AccessSplitter
{
    public class LogContainer
    {
        private string _ip;
        private GeoCoordinate _loc;
        private DateTime _date;
        private string _url;
        private string _requesttype;
        private int _httpCode;
        private string _useragent;
        private string _httpversion;

        public string Ip => _ip;

        public GeoCoordinate Loc
        {
            get => _loc;
            set => _loc = value;
        }

        public DateTime Date => _date;

        public string Url => _url;

        public int HttpCode => _httpCode;

        public string Useragent => _useragent;

        public string Requesttype => _requesttype;

        public string Httpversion => _httpversion;

        public LogContainer(string ip, GeoCoordinate loc, DateTime date, string url, int httpCode, string useragent, string requesttype, string httpversion)
        {
            _ip = ip;
            _loc = loc;
            _date = date;
            _url = url;
            _httpCode = httpCode;
            _useragent = useragent;
            _requesttype = requesttype;
            _httpversion = httpversion;
        }
    }
}